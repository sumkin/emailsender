from distutils.core import setup

setup(
    name='EmailSender',
    version='0.0.4',
    author='Fedor Nikitin',
    author_email='fedor.nikitin@gmail.com',
    packages=['emailsender'],
    description='EmailSender package',
)
