import os
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders


class EmailSender:


    def __init__(self,server,port,user,passwd):
        self.prot = 'SMTP'
        self.server = server
        self.port   = port
        self.user   = user
        self.passwd = passwd


    def send_quick(self,frm,to,subj,text):
        msg = MIMEText(text)
        msg['Subject'] = subj
        msg['From'] = frm
        msg['To'] = to

        s = smtplib.SMTP(self.server,self.port)
        s.starttls()
        s.login(self.user, self.passwd)
        s.sendmail(frm,to,msg.as_string())  


    def send_html(self,frm,to,subj,html):
        msg = MIMEMultipart('alternative')
        msg['Subject'] = subj
        msg['From'] = frm
        msg['To'] = to
        txt = html
    
        part1 = MIMEText(txt,'plain')
        part2 = MIMEText(html,'html')

        msg.attach(part1)
        msg.attach(part2)

        s = smtplib.SMTP(self.server,self.port)
        s.starttls()
        s.login(self.user, self.passwd)
        s.sendmail(frm,to,msg.as_string())
        s.quit()


    def send_multipart(self,frm,to,subj,text,files):
        msg = MIMEMultipart()
        msg['From'] = frm
        msg['To'] = to
        msg['Subject'] = subj

        msg.attach(MIMEText(text))
        for f in files:
            part = MIMEBase('application','octet-stream')
            part.set_payload(open(f,'rb').read())
            encoders.encode_base64(part)       
            part.add_header("Content-Disposition","attachment; filename=%s" % os.path.basename(f))
            msg.attach(part)
 
        s = smtplib.SMTP(self.server,self.port)
        s.starttls()
        s.login(self.user, self.passwd)
        s.sendmail(frm,to,msg.as_string()) 


if __name__ == '__main__':
    es = EmailSender('email-smtp.eu-west-1.amazonaws.com',25,'AKIAITNWXVR77FGGINLA','ArOwR7uG5Tovy/4xJkUVs4gLgsi7dw8zACluZ3t39m+C')
    es.send_quick('fedor.nikitin@finnair.com','fedor.nikitin@finnair.com','sbj','txt')




